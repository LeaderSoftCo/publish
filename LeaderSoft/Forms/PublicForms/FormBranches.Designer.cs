﻿namespace LeaderSoft.Forms.PublicForms
{
    partial class FormBranches
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(FormBranches));
            this.panel1 = new System.Windows.Forms.Panel();
            this.lableTitle = new System.Windows.Forms.Label();
            this.PictureBoxTitle = new System.Windows.Forms.PictureBox();
            this.TreeViewMain = new System.Windows.Forms.TreeView();
            this.panel2 = new System.Windows.Forms.Panel();
            this.buttonGroupSearch = new System.Windows.Forms.Button();
            this.RadioBtnNotGroup = new System.Windows.Forms.RadioButton();
            this.RadioBtnGroup = new System.Windows.Forms.RadioButton();
            this.TextBoxNameLng2 = new System.Windows.Forms.TextBox();
            this.TextBoxNameLng1 = new System.Windows.Forms.TextBox();
            this.labelNameLng2 = new System.Windows.Forms.Label();
            this.labelNameLng1 = new System.Windows.Forms.Label();
            this.TextBoxGroupName = new System.Windows.Forms.TextBox();
            this.labelGroupName = new System.Windows.Forms.Label();
            this.TextBoxGroupID = new System.Windows.Forms.TextBox();
            this.labelGroupID = new System.Windows.Forms.Label();
            this.TextBoxID = new System.Windows.Forms.TextBox();
            this.buttonMainSearch = new System.Windows.Forms.Button();
            this.TextBoxMainName = new System.Windows.Forms.TextBox();
            this.TextBoxMainID = new System.Windows.Forms.TextBox();
            this.labelID = new System.Windows.Forms.Label();
            this.labelMain = new System.Windows.Forms.Label();
            this.ButtonPrevious = new System.Windows.Forms.Button();
            this.ButtonNext = new System.Windows.Forms.Button();
            this.ButtonFirst = new System.Windows.Forms.Button();
            this.ButtonLast = new System.Windows.Forms.Button();
            this.Pan = new System.Windows.Forms.Panel();
            this.ButtonExit = new System.Windows.Forms.Button();
            this.ButtonCancel = new System.Windows.Forms.Button();
            this.ButtonSearch = new System.Windows.Forms.Button();
            this.ButtonDelete = new System.Windows.Forms.Button();
            this.ButtonEdit = new System.Windows.Forms.Button();
            this.ButtonPrint = new System.Windows.Forms.Button();
            this.ButtonSave = new System.Windows.Forms.Button();
            this.ButtonNew = new System.Windows.Forms.Button();
            this.re = new System.Windows.Forms.PictureBox();
            this.panel4 = new System.Windows.Forms.Panel();
            this.TxtBoxAdvocatteshipExpDate = new System.Windows.Forms.TextBox();
            this.textBox1 = new System.Windows.Forms.TextBox();
            this.TxtBoxAdvocatteshipLssDate = new System.Windows.Forms.TextBox();
            this.TxtBoxAdvocatteship = new System.Windows.Forms.TextBox();
            this.TextBoxMunicpExpDate = new System.Windows.Forms.TextBox();
            this.TextBoxMunicpLssDate = new System.Windows.Forms.TextBox();
            this.TextBoxMunicp = new System.Windows.Forms.TextBox();
            this.TextBoxCrExpDate = new System.Windows.Forms.TextBox();
            this.TextBoxCrLssDate = new System.Windows.Forms.TextBox();
            this.TextBoxCrNO = new System.Windows.Forms.TextBox();
            this.LablCrExpDate3 = new System.Windows.Forms.Label();
            this.LablCrLssDate3 = new System.Windows.Forms.Label();
            this.LablAdvocateship = new System.Windows.Forms.Label();
            this.LablCrExpDate2 = new System.Windows.Forms.Label();
            this.LablCrLssDate2 = new System.Windows.Forms.Label();
            this.LablMunicp = new System.Windows.Forms.Label();
            this.LablCrExpDate1 = new System.Windows.Forms.Label();
            this.LablCrLssDate1 = new System.Windows.Forms.Label();
            this.LablCrNo = new System.Windows.Forms.Label();
            this.panel3 = new System.Windows.Forms.Panel();
            this.label2 = new System.Windows.Forms.Label();
            this.label1 = new System.Windows.Forms.Label();
            this.TextBoxRentEndDate = new System.Windows.Forms.TextBox();
            this.TextBoxRentStartDate = new System.Windows.Forms.TextBox();
            this.TextBoxAddress = new System.Windows.Forms.TextBox();
            this.LableAddress = new System.Windows.Forms.Label();
            this.TextBoxeFax = new System.Windows.Forms.TextBox();
            this.LableFax = new System.Windows.Forms.Label();
            this.TextBoxPhone = new System.Windows.Forms.TextBox();
            this.LablePhone = new System.Windows.Forms.Label();
            this.panel1.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.PictureBoxTitle)).BeginInit();
            this.panel2.SuspendLayout();
            this.Pan.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.re)).BeginInit();
            this.panel4.SuspendLayout();
            this.panel3.SuspendLayout();
            this.SuspendLayout();
            // 
            // panel1
            // 
            this.panel1.Controls.Add(this.lableTitle);
            this.panel1.Controls.Add(this.PictureBoxTitle);
            this.panel1.Location = new System.Drawing.Point(3, 1);
            this.panel1.Margin = new System.Windows.Forms.Padding(4, 4, 4, 4);
            this.panel1.Name = "panel1";
            this.panel1.Size = new System.Drawing.Size(1062, 79);
            this.panel1.TabIndex = 41;
            // 
            // lableTitle
            // 
            this.lableTitle.AutoSize = true;
            this.lableTitle.Location = new System.Drawing.Point(601, 32);
            this.lableTitle.Margin = new System.Windows.Forms.Padding(4, 0, 4, 0);
            this.lableTitle.Name = "lableTitle";
            this.lableTitle.Size = new System.Drawing.Size(73, 19);
            this.lableTitle.TabIndex = 10;
            this.lableTitle.Text = "lableTitle";
            // 
            // PictureBoxTitle
            // 
            this.PictureBoxTitle.Location = new System.Drawing.Point(462, 4);
            this.PictureBoxTitle.Margin = new System.Windows.Forms.Padding(4, 4, 4, 4);
            this.PictureBoxTitle.Name = "PictureBoxTitle";
            this.PictureBoxTitle.Size = new System.Drawing.Size(98, 67);
            this.PictureBoxTitle.TabIndex = 9;
            this.PictureBoxTitle.TabStop = false;
            // 
            // TreeViewMain
            // 
            this.TreeViewMain.Location = new System.Drawing.Point(3, 89);
            this.TreeViewMain.Margin = new System.Windows.Forms.Padding(4, 4, 4, 4);
            this.TreeViewMain.Name = "TreeViewMain";
            this.TreeViewMain.Size = new System.Drawing.Size(290, 510);
            this.TreeViewMain.TabIndex = 40;
            this.TreeViewMain.AfterSelect += new System.Windows.Forms.TreeViewEventHandler(this.TreeViewMain_AfterSelect);
            // 
            // panel2
            // 
            this.panel2.Controls.Add(this.buttonGroupSearch);
            this.panel2.Controls.Add(this.RadioBtnNotGroup);
            this.panel2.Controls.Add(this.RadioBtnGroup);
            this.panel2.Controls.Add(this.TextBoxNameLng2);
            this.panel2.Controls.Add(this.TextBoxNameLng1);
            this.panel2.Controls.Add(this.labelNameLng2);
            this.panel2.Controls.Add(this.labelNameLng1);
            this.panel2.Controls.Add(this.TextBoxGroupName);
            this.panel2.Controls.Add(this.labelGroupName);
            this.panel2.Controls.Add(this.TextBoxGroupID);
            this.panel2.Controls.Add(this.labelGroupID);
            this.panel2.Controls.Add(this.TextBoxID);
            this.panel2.Controls.Add(this.buttonMainSearch);
            this.panel2.Controls.Add(this.TextBoxMainName);
            this.panel2.Controls.Add(this.TextBoxMainID);
            this.panel2.Controls.Add(this.labelID);
            this.panel2.Controls.Add(this.labelMain);
            this.panel2.Location = new System.Drawing.Point(300, 89);
            this.panel2.Margin = new System.Windows.Forms.Padding(4, 4, 4, 4);
            this.panel2.Name = "panel2";
            this.panel2.Size = new System.Drawing.Size(753, 219);
            this.panel2.TabIndex = 39;
            // 
            // buttonGroupSearch
            // 
            this.buttonGroupSearch.FlatAppearance.MouseDownBackColor = System.Drawing.Color.White;
            this.buttonGroupSearch.Image = global::LeaderSoft.Properties.Resources.More_20px;
            this.buttonGroupSearch.Location = new System.Drawing.Point(270, 70);
            this.buttonGroupSearch.Margin = new System.Windows.Forms.Padding(4, 4, 4, 4);
            this.buttonGroupSearch.Name = "buttonGroupSearch";
            this.buttonGroupSearch.Size = new System.Drawing.Size(28, 29);
            this.buttonGroupSearch.TabIndex = 92;
            this.buttonGroupSearch.UseVisualStyleBackColor = true;
            this.buttonGroupSearch.Click += new System.EventHandler(this.buttonGroupSearch_Click);
            // 
            // RadioBtnNotGroup
            // 
            this.RadioBtnNotGroup.AutoSize = true;
            this.RadioBtnNotGroup.Location = new System.Drawing.Point(369, 187);
            this.RadioBtnNotGroup.Margin = new System.Windows.Forms.Padding(4, 4, 4, 4);
            this.RadioBtnNotGroup.Name = "RadioBtnNotGroup";
            this.RadioBtnNotGroup.Size = new System.Drawing.Size(69, 23);
            this.RadioBtnNotGroup.TabIndex = 91;
            this.RadioBtnNotGroup.TabStop = true;
            this.RadioBtnNotGroup.Text = "فردي";
            this.RadioBtnNotGroup.UseVisualStyleBackColor = true;
            // 
            // RadioBtnGroup
            // 
            this.RadioBtnGroup.AutoSize = true;
            this.RadioBtnGroup.Location = new System.Drawing.Point(516, 187);
            this.RadioBtnGroup.Margin = new System.Windows.Forms.Padding(4, 4, 4, 4);
            this.RadioBtnGroup.Name = "RadioBtnGroup";
            this.RadioBtnGroup.Size = new System.Drawing.Size(88, 23);
            this.RadioBtnGroup.TabIndex = 90;
            this.RadioBtnGroup.TabStop = true;
            this.RadioBtnGroup.Text = "مجموعة";
            this.RadioBtnGroup.UseVisualStyleBackColor = true;
            // 
            // TextBoxNameLng2
            // 
            this.TextBoxNameLng2.Location = new System.Drawing.Point(9, 149);
            this.TextBoxNameLng2.Margin = new System.Windows.Forms.Padding(4, 4, 4, 4);
            this.TextBoxNameLng2.Name = "TextBoxNameLng2";
            this.TextBoxNameLng2.Size = new System.Drawing.Size(601, 27);
            this.TextBoxNameLng2.TabIndex = 13;
            // 
            // TextBoxNameLng1
            // 
            this.TextBoxNameLng1.Location = new System.Drawing.Point(9, 104);
            this.TextBoxNameLng1.Margin = new System.Windows.Forms.Padding(4, 4, 4, 4);
            this.TextBoxNameLng1.Name = "TextBoxNameLng1";
            this.TextBoxNameLng1.Size = new System.Drawing.Size(601, 27);
            this.TextBoxNameLng1.TabIndex = 12;
            // 
            // labelNameLng2
            // 
            this.labelNameLng2.AutoSize = true;
            this.labelNameLng2.Location = new System.Drawing.Point(620, 151);
            this.labelNameLng2.Margin = new System.Windows.Forms.Padding(4, 0, 4, 0);
            this.labelNameLng2.Name = "labelNameLng2";
            this.labelNameLng2.Size = new System.Drawing.Size(122, 19);
            this.labelNameLng2.TabIndex = 11;
            this.labelNameLng2.Text = "الاسم بالانجليزي";
            // 
            // labelNameLng1
            // 
            this.labelNameLng1.AutoSize = true;
            this.labelNameLng1.Location = new System.Drawing.Point(622, 105);
            this.labelNameLng1.Margin = new System.Windows.Forms.Padding(4, 0, 4, 0);
            this.labelNameLng1.Name = "labelNameLng1";
            this.labelNameLng1.Size = new System.Drawing.Size(104, 19);
            this.labelNameLng1.TabIndex = 10;
            this.labelNameLng1.Text = "الاسم بالعربي";
            // 
            // TextBoxGroupName
            // 
            this.TextBoxGroupName.Location = new System.Drawing.Point(9, 69);
            this.TextBoxGroupName.Margin = new System.Windows.Forms.Padding(4, 4, 4, 4);
            this.TextBoxGroupName.Name = "TextBoxGroupName";
            this.TextBoxGroupName.Size = new System.Drawing.Size(154, 27);
            this.TextBoxGroupName.TabIndex = 9;
            // 
            // labelGroupName
            // 
            this.labelGroupName.AutoSize = true;
            this.labelGroupName.Location = new System.Drawing.Point(166, 73);
            this.labelGroupName.Margin = new System.Windows.Forms.Padding(4, 0, 4, 0);
            this.labelGroupName.Name = "labelGroupName";
            this.labelGroupName.Size = new System.Drawing.Size(100, 19);
            this.labelGroupName.TabIndex = 8;
            this.labelGroupName.Text = "اسم الجموعة";
            // 
            // TextBoxGroupID
            // 
            this.TextBoxGroupID.Location = new System.Drawing.Point(298, 69);
            this.TextBoxGroupID.Margin = new System.Windows.Forms.Padding(4, 4, 4, 4);
            this.TextBoxGroupID.Name = "TextBoxGroupID";
            this.TextBoxGroupID.Size = new System.Drawing.Size(85, 27);
            this.TextBoxGroupID.TabIndex = 7;
            this.TextBoxGroupID.Leave += new System.EventHandler(this.TextBoxGroupID_Leave);
            // 
            // labelGroupID
            // 
            this.labelGroupID.AutoSize = true;
            this.labelGroupID.Location = new System.Drawing.Point(384, 73);
            this.labelGroupID.Margin = new System.Windows.Forms.Padding(4, 0, 4, 0);
            this.labelGroupID.Name = "labelGroupID";
            this.labelGroupID.Size = new System.Drawing.Size(103, 19);
            this.labelGroupID.TabIndex = 6;
            this.labelGroupID.Text = "رقم المجموعة";
            // 
            // TextBoxID
            // 
            this.TextBoxID.Location = new System.Drawing.Point(501, 69);
            this.TextBoxID.Margin = new System.Windows.Forms.Padding(4, 4, 4, 4);
            this.TextBoxID.Name = "TextBoxID";
            this.TextBoxID.Size = new System.Drawing.Size(109, 27);
            this.TextBoxID.TabIndex = 5;
            this.TextBoxID.Leave += new System.EventHandler(this.TextBoxID_Leave);
            // 
            // buttonMainSearch
            // 
            this.buttonMainSearch.FlatAppearance.MouseDownBackColor = System.Drawing.Color.White;
            this.buttonMainSearch.Image = global::LeaderSoft.Properties.Resources.More_20px;
            this.buttonMainSearch.Location = new System.Drawing.Point(464, 19);
            this.buttonMainSearch.Margin = new System.Windows.Forms.Padding(4, 4, 4, 4);
            this.buttonMainSearch.Name = "buttonMainSearch";
            this.buttonMainSearch.Size = new System.Drawing.Size(28, 29);
            this.buttonMainSearch.TabIndex = 4;
            this.buttonMainSearch.UseVisualStyleBackColor = true;
            // 
            // TextBoxMainName
            // 
            this.TextBoxMainName.Location = new System.Drawing.Point(9, 19);
            this.TextBoxMainName.Margin = new System.Windows.Forms.Padding(4, 4, 4, 4);
            this.TextBoxMainName.Name = "TextBoxMainName";
            this.TextBoxMainName.Size = new System.Drawing.Size(444, 27);
            this.TextBoxMainName.TabIndex = 3;
            // 
            // TextBoxMainID
            // 
            this.TextBoxMainID.Location = new System.Drawing.Point(501, 18);
            this.TextBoxMainID.Margin = new System.Windows.Forms.Padding(4, 4, 4, 4);
            this.TextBoxMainID.Name = "TextBoxMainID";
            this.TextBoxMainID.Size = new System.Drawing.Size(109, 27);
            this.TextBoxMainID.TabIndex = 2;
            // 
            // labelID
            // 
            this.labelID.AutoSize = true;
            this.labelID.Location = new System.Drawing.Point(638, 69);
            this.labelID.Margin = new System.Windows.Forms.Padding(4, 0, 4, 0);
            this.labelID.Name = "labelID";
            this.labelID.Size = new System.Drawing.Size(70, 19);
            this.labelID.TabIndex = 1;
            this.labelID.Text = "رقم الكود";
            // 
            // labelMain
            // 
            this.labelMain.AutoSize = true;
            this.labelMain.Location = new System.Drawing.Point(614, 22);
            this.labelMain.Margin = new System.Windows.Forms.Padding(4, 0, 4, 0);
            this.labelMain.Name = "labelMain";
            this.labelMain.Size = new System.Drawing.Size(128, 19);
            this.labelMain.TabIndex = 0;
            this.labelMain.Text = "الحساب الرئيسي";
            // 
            // ButtonPrevious
            // 
            this.ButtonPrevious.FlatAppearance.BorderSize = 0;
            this.ButtonPrevious.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.ButtonPrevious.Image = ((System.Drawing.Image)(resources.GetObject("ButtonPrevious.Image")));
            this.ButtonPrevious.Location = new System.Drawing.Point(478, 609);
            this.ButtonPrevious.Margin = new System.Windows.Forms.Padding(4, 4, 4, 4);
            this.ButtonPrevious.Name = "ButtonPrevious";
            this.ButtonPrevious.Size = new System.Drawing.Size(38, 76);
            this.ButtonPrevious.TabIndex = 87;
            this.ButtonPrevious.UseVisualStyleBackColor = true;
            this.ButtonPrevious.Click += new System.EventHandler(this.ButtonPrevious_Click);
            // 
            // ButtonNext
            // 
            this.ButtonNext.FlatAppearance.BorderSize = 0;
            this.ButtonNext.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.ButtonNext.Image = ((System.Drawing.Image)(resources.GetObject("ButtonNext.Image")));
            this.ButtonNext.Location = new System.Drawing.Point(765, 609);
            this.ButtonNext.Margin = new System.Windows.Forms.Padding(4, 4, 4, 4);
            this.ButtonNext.Name = "ButtonNext";
            this.ButtonNext.Size = new System.Drawing.Size(38, 76);
            this.ButtonNext.TabIndex = 88;
            this.ButtonNext.UseVisualStyleBackColor = true;
            this.ButtonNext.Click += new System.EventHandler(this.ButtonNext_Click);
            // 
            // ButtonFirst
            // 
            this.ButtonFirst.FlatAppearance.BorderSize = 0;
            this.ButtonFirst.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.ButtonFirst.Image = ((System.Drawing.Image)(resources.GetObject("ButtonFirst.Image")));
            this.ButtonFirst.Location = new System.Drawing.Point(339, 617);
            this.ButtonFirst.Margin = new System.Windows.Forms.Padding(4, 4, 4, 4);
            this.ButtonFirst.Name = "ButtonFirst";
            this.ButtonFirst.Size = new System.Drawing.Size(80, 63);
            this.ButtonFirst.TabIndex = 89;
            this.ButtonFirst.UseVisualStyleBackColor = true;
            // 
            // ButtonLast
            // 
            this.ButtonLast.FlatAppearance.BorderSize = 0;
            this.ButtonLast.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.ButtonLast.Image = ((System.Drawing.Image)(resources.GetObject("ButtonLast.Image")));
            this.ButtonLast.Location = new System.Drawing.Point(849, 623);
            this.ButtonLast.Margin = new System.Windows.Forms.Padding(4, 4, 4, 4);
            this.ButtonLast.Name = "ButtonLast";
            this.ButtonLast.Size = new System.Drawing.Size(80, 63);
            this.ButtonLast.TabIndex = 90;
            this.ButtonLast.UseVisualStyleBackColor = true;
            this.ButtonLast.Click += new System.EventHandler(this.ButtonLast_Click);
            // 
            // Pan
            // 
            this.Pan.BackColor = System.Drawing.SystemColors.AppWorkspace;
            this.Pan.Controls.Add(this.ButtonExit);
            this.Pan.Controls.Add(this.ButtonCancel);
            this.Pan.Controls.Add(this.ButtonSearch);
            this.Pan.Controls.Add(this.ButtonDelete);
            this.Pan.Controls.Add(this.ButtonEdit);
            this.Pan.Controls.Add(this.ButtonPrint);
            this.Pan.Controls.Add(this.ButtonSave);
            this.Pan.Controls.Add(this.ButtonNew);
            this.Pan.Controls.Add(this.re);
            this.Pan.Dock = System.Windows.Forms.DockStyle.Right;
            this.Pan.Location = new System.Drawing.Point(1284, 0);
            this.Pan.Margin = new System.Windows.Forms.Padding(4, 4, 4, 4);
            this.Pan.Name = "Pan";
            this.Pan.Size = new System.Drawing.Size(50, 722);
            this.Pan.TabIndex = 91;
            // 
            // ButtonExit
            // 
            this.ButtonExit.Location = new System.Drawing.Point(12, 574);
            this.ButtonExit.Margin = new System.Windows.Forms.Padding(4, 4, 4, 4);
            this.ButtonExit.Name = "ButtonExit";
            this.ButtonExit.Size = new System.Drawing.Size(230, 54);
            this.ButtonExit.TabIndex = 7;
            this.ButtonExit.Text = "خروج";
            this.ButtonExit.UseVisualStyleBackColor = true;
            this.ButtonExit.Click += new System.EventHandler(this.ButtonExit_Click);
            // 
            // ButtonCancel
            // 
            this.ButtonCancel.Location = new System.Drawing.Point(12, 517);
            this.ButtonCancel.Margin = new System.Windows.Forms.Padding(4, 4, 4, 4);
            this.ButtonCancel.Name = "ButtonCancel";
            this.ButtonCancel.Size = new System.Drawing.Size(230, 54);
            this.ButtonCancel.TabIndex = 7;
            this.ButtonCancel.Text = "الغاء";
            this.ButtonCancel.UseVisualStyleBackColor = true;
            this.ButtonCancel.Click += new System.EventHandler(this.ButtonCancel_Click);
            // 
            // ButtonSearch
            // 
            this.ButtonSearch.Location = new System.Drawing.Point(12, 460);
            this.ButtonSearch.Margin = new System.Windows.Forms.Padding(4, 4, 4, 4);
            this.ButtonSearch.Name = "ButtonSearch";
            this.ButtonSearch.Size = new System.Drawing.Size(230, 54);
            this.ButtonSearch.TabIndex = 7;
            this.ButtonSearch.Text = "بحث";
            this.ButtonSearch.UseVisualStyleBackColor = true;
            this.ButtonSearch.Click += new System.EventHandler(this.ButtonSearch_Click);
            // 
            // ButtonDelete
            // 
            this.ButtonDelete.Location = new System.Drawing.Point(12, 348);
            this.ButtonDelete.Margin = new System.Windows.Forms.Padding(4, 4, 4, 4);
            this.ButtonDelete.Name = "ButtonDelete";
            this.ButtonDelete.Size = new System.Drawing.Size(230, 54);
            this.ButtonDelete.TabIndex = 7;
            this.ButtonDelete.Text = "حذف";
            this.ButtonDelete.UseVisualStyleBackColor = true;
            this.ButtonDelete.Click += new System.EventHandler(this.ButtonDelete_Click);
            // 
            // ButtonEdit
            // 
            this.ButtonEdit.Location = new System.Drawing.Point(12, 237);
            this.ButtonEdit.Margin = new System.Windows.Forms.Padding(4, 4, 4, 4);
            this.ButtonEdit.Name = "ButtonEdit";
            this.ButtonEdit.Size = new System.Drawing.Size(230, 54);
            this.ButtonEdit.TabIndex = 7;
            this.ButtonEdit.Text = "تعديل";
            this.ButtonEdit.UseVisualStyleBackColor = true;
            this.ButtonEdit.Click += new System.EventHandler(this.ButtonEdit_Click);
            // 
            // ButtonPrint
            // 
            this.ButtonPrint.Location = new System.Drawing.Point(12, 403);
            this.ButtonPrint.Margin = new System.Windows.Forms.Padding(4, 4, 4, 4);
            this.ButtonPrint.Name = "ButtonPrint";
            this.ButtonPrint.Size = new System.Drawing.Size(230, 54);
            this.ButtonPrint.TabIndex = 7;
            this.ButtonPrint.Text = "طباعة";
            this.ButtonPrint.UseVisualStyleBackColor = true;
            this.ButtonPrint.Click += new System.EventHandler(this.ButtonPrint_Click);
            // 
            // ButtonSave
            // 
            this.ButtonSave.Location = new System.Drawing.Point(12, 294);
            this.ButtonSave.Margin = new System.Windows.Forms.Padding(4, 4, 4, 4);
            this.ButtonSave.Name = "ButtonSave";
            this.ButtonSave.Size = new System.Drawing.Size(230, 54);
            this.ButtonSave.TabIndex = 7;
            this.ButtonSave.Text = "حفظ";
            this.ButtonSave.UseVisualStyleBackColor = true;
            this.ButtonSave.Click += new System.EventHandler(this.ButtonSave_Click);
            // 
            // ButtonNew
            // 
            this.ButtonNew.Location = new System.Drawing.Point(12, 183);
            this.ButtonNew.Margin = new System.Windows.Forms.Padding(4, 4, 4, 4);
            this.ButtonNew.Name = "ButtonNew";
            this.ButtonNew.Size = new System.Drawing.Size(230, 54);
            this.ButtonNew.TabIndex = 7;
            this.ButtonNew.Text = "جديد";
            this.ButtonNew.UseVisualStyleBackColor = true;
            this.ButtonNew.Click += new System.EventHandler(this.ButtonNew_Click);
            // 
            // re
            // 
            this.re.Image = ((System.Drawing.Image)(resources.GetObject("re.Image")));
            this.re.Location = new System.Drawing.Point(8, 4);
            this.re.Margin = new System.Windows.Forms.Padding(4, 4, 4, 4);
            this.re.Name = "re";
            this.re.Size = new System.Drawing.Size(39, 37);
            this.re.SizeMode = System.Windows.Forms.PictureBoxSizeMode.Zoom;
            this.re.TabIndex = 6;
            this.re.TabStop = false;
            this.re.Click += new System.EventHandler(this.re_Click);
            // 
            // panel4
            // 
            this.panel4.Controls.Add(this.TxtBoxAdvocatteshipExpDate);
            this.panel4.Controls.Add(this.textBox1);
            this.panel4.Controls.Add(this.TxtBoxAdvocatteshipLssDate);
            this.panel4.Controls.Add(this.TxtBoxAdvocatteship);
            this.panel4.Controls.Add(this.TextBoxMunicpExpDate);
            this.panel4.Controls.Add(this.TextBoxMunicpLssDate);
            this.panel4.Controls.Add(this.TextBoxMunicp);
            this.panel4.Controls.Add(this.TextBoxCrExpDate);
            this.panel4.Controls.Add(this.TextBoxCrLssDate);
            this.panel4.Controls.Add(this.TextBoxCrNO);
            this.panel4.Controls.Add(this.LablCrExpDate3);
            this.panel4.Controls.Add(this.LablCrLssDate3);
            this.panel4.Controls.Add(this.LablAdvocateship);
            this.panel4.Controls.Add(this.LablCrExpDate2);
            this.panel4.Controls.Add(this.LablCrLssDate2);
            this.panel4.Controls.Add(this.LablMunicp);
            this.panel4.Controls.Add(this.LablCrExpDate1);
            this.panel4.Controls.Add(this.LablCrLssDate1);
            this.panel4.Controls.Add(this.LablCrNo);
            this.panel4.Location = new System.Drawing.Point(300, 460);
            this.panel4.Margin = new System.Windows.Forms.Padding(4, 4, 4, 4);
            this.panel4.Name = "panel4";
            this.panel4.Size = new System.Drawing.Size(753, 140);
            this.panel4.TabIndex = 51;
            // 
            // TxtBoxAdvocatteshipExpDate
            // 
            this.TxtBoxAdvocatteshipExpDate.Location = new System.Drawing.Point(4, 95);
            this.TxtBoxAdvocatteshipExpDate.Margin = new System.Windows.Forms.Padding(4, 4, 4, 4);
            this.TxtBoxAdvocatteshipExpDate.Name = "TxtBoxAdvocatteshipExpDate";
            this.TxtBoxAdvocatteshipExpDate.Size = new System.Drawing.Size(133, 27);
            this.TxtBoxAdvocatteshipExpDate.TabIndex = 19;
            // 
            // textBox1
            // 
            this.textBox1.Location = new System.Drawing.Point(-136, 140);
            this.textBox1.Margin = new System.Windows.Forms.Padding(4, 4, 4, 4);
            this.textBox1.Name = "textBox1";
            this.textBox1.Size = new System.Drawing.Size(106, 27);
            this.textBox1.TabIndex = 64;
            // 
            // TxtBoxAdvocatteshipLssDate
            // 
            this.TxtBoxAdvocatteshipLssDate.Location = new System.Drawing.Point(4, 50);
            this.TxtBoxAdvocatteshipLssDate.Margin = new System.Windows.Forms.Padding(4, 4, 4, 4);
            this.TxtBoxAdvocatteshipLssDate.Name = "TxtBoxAdvocatteshipLssDate";
            this.TxtBoxAdvocatteshipLssDate.Size = new System.Drawing.Size(133, 27);
            this.TxtBoxAdvocatteshipLssDate.TabIndex = 18;
            // 
            // TxtBoxAdvocatteship
            // 
            this.TxtBoxAdvocatteship.Location = new System.Drawing.Point(4, 12);
            this.TxtBoxAdvocatteship.Margin = new System.Windows.Forms.Padding(4, 4, 4, 4);
            this.TxtBoxAdvocatteship.Name = "TxtBoxAdvocatteship";
            this.TxtBoxAdvocatteship.Size = new System.Drawing.Size(133, 27);
            this.TxtBoxAdvocatteship.TabIndex = 17;
            // 
            // TextBoxMunicpExpDate
            // 
            this.TextBoxMunicpExpDate.Location = new System.Drawing.Point(294, 91);
            this.TextBoxMunicpExpDate.Margin = new System.Windows.Forms.Padding(4, 4, 4, 4);
            this.TextBoxMunicpExpDate.Name = "TextBoxMunicpExpDate";
            this.TextBoxMunicpExpDate.Size = new System.Drawing.Size(126, 27);
            this.TextBoxMunicpExpDate.TabIndex = 16;
            // 
            // TextBoxMunicpLssDate
            // 
            this.TextBoxMunicpLssDate.Location = new System.Drawing.Point(294, 51);
            this.TextBoxMunicpLssDate.Margin = new System.Windows.Forms.Padding(4, 4, 4, 4);
            this.TextBoxMunicpLssDate.Name = "TextBoxMunicpLssDate";
            this.TextBoxMunicpLssDate.Size = new System.Drawing.Size(126, 27);
            this.TextBoxMunicpLssDate.TabIndex = 15;
            // 
            // TextBoxMunicp
            // 
            this.TextBoxMunicp.Location = new System.Drawing.Point(294, 13);
            this.TextBoxMunicp.Margin = new System.Windows.Forms.Padding(4, 4, 4, 4);
            this.TextBoxMunicp.Name = "TextBoxMunicp";
            this.TextBoxMunicp.Size = new System.Drawing.Size(126, 27);
            this.TextBoxMunicp.TabIndex = 14;
            // 
            // TextBoxCrExpDate
            // 
            this.TextBoxCrExpDate.Location = new System.Drawing.Point(528, 91);
            this.TextBoxCrExpDate.Margin = new System.Windows.Forms.Padding(4, 4, 4, 4);
            this.TextBoxCrExpDate.Name = "TextBoxCrExpDate";
            this.TextBoxCrExpDate.Size = new System.Drawing.Size(106, 27);
            this.TextBoxCrExpDate.TabIndex = 13;
            // 
            // TextBoxCrLssDate
            // 
            this.TextBoxCrLssDate.Location = new System.Drawing.Point(528, 50);
            this.TextBoxCrLssDate.Margin = new System.Windows.Forms.Padding(4, 4, 4, 4);
            this.TextBoxCrLssDate.Name = "TextBoxCrLssDate";
            this.TextBoxCrLssDate.Size = new System.Drawing.Size(106, 27);
            this.TextBoxCrLssDate.TabIndex = 12;
            // 
            // TextBoxCrNO
            // 
            this.TextBoxCrNO.Location = new System.Drawing.Point(528, 12);
            this.TextBoxCrNO.Margin = new System.Windows.Forms.Padding(4, 4, 4, 4);
            this.TextBoxCrNO.Name = "TextBoxCrNO";
            this.TextBoxCrNO.Size = new System.Drawing.Size(106, 27);
            this.TextBoxCrNO.TabIndex = 11;
            // 
            // LablCrExpDate3
            // 
            this.LablCrExpDate3.AutoSize = true;
            this.LablCrExpDate3.Location = new System.Drawing.Point(172, 95);
            this.LablCrExpDate3.Margin = new System.Windows.Forms.Padding(4, 0, 4, 0);
            this.LablCrExpDate3.Name = "LablCrExpDate3";
            this.LablCrExpDate3.Size = new System.Drawing.Size(88, 19);
            this.LablCrExpDate3.TabIndex = 55;
            this.LablCrExpDate3.Text = "تاريخ الانتهاء";
            // 
            // LablCrLssDate3
            // 
            this.LablCrLssDate3.AutoSize = true;
            this.LablCrLssDate3.Location = new System.Drawing.Point(172, 54);
            this.LablCrLssDate3.Margin = new System.Windows.Forms.Padding(4, 0, 4, 0);
            this.LablCrLssDate3.Name = "LablCrLssDate3";
            this.LablCrLssDate3.Size = new System.Drawing.Size(89, 19);
            this.LablCrLssDate3.TabIndex = 54;
            this.LablCrLssDate3.Text = "تاريخ الاصدار";
            // 
            // LablAdvocateship
            // 
            this.LablAdvocateship.AutoSize = true;
            this.LablAdvocateship.Location = new System.Drawing.Point(144, 19);
            this.LablAdvocateship.Margin = new System.Windows.Forms.Padding(4, 0, 4, 0);
            this.LablAdvocateship.Name = "LablAdvocateship";
            this.LablAdvocateship.Size = new System.Drawing.Size(145, 19);
            this.LablAdvocateship.TabIndex = 53;
            this.LablAdvocateship.Text = "رخصة الدفاع المدني";
            // 
            // LablCrExpDate2
            // 
            this.LablCrExpDate2.AutoSize = true;
            this.LablCrExpDate2.Location = new System.Drawing.Point(429, 95);
            this.LablCrExpDate2.Margin = new System.Windows.Forms.Padding(4, 0, 4, 0);
            this.LablCrExpDate2.Name = "LablCrExpDate2";
            this.LablCrExpDate2.Size = new System.Drawing.Size(88, 19);
            this.LablCrExpDate2.TabIndex = 52;
            this.LablCrExpDate2.Text = "تاريخ الانتهاء";
            // 
            // LablCrLssDate2
            // 
            this.LablCrLssDate2.AutoSize = true;
            this.LablCrLssDate2.Location = new System.Drawing.Point(426, 56);
            this.LablCrLssDate2.Margin = new System.Windows.Forms.Padding(4, 0, 4, 0);
            this.LablCrLssDate2.Name = "LablCrLssDate2";
            this.LablCrLssDate2.Size = new System.Drawing.Size(89, 19);
            this.LablCrLssDate2.TabIndex = 51;
            this.LablCrLssDate2.Text = "تاريخ الاصدار";
            // 
            // LablMunicp
            // 
            this.LablMunicp.AutoSize = true;
            this.LablMunicp.Location = new System.Drawing.Point(429, 20);
            this.LablMunicp.Margin = new System.Windows.Forms.Padding(4, 0, 4, 0);
            this.LablMunicp.Name = "LablMunicp";
            this.LablMunicp.Size = new System.Drawing.Size(95, 19);
            this.LablMunicp.TabIndex = 50;
            this.LablMunicp.Text = "رخصة البلدية";
            // 
            // LablCrExpDate1
            // 
            this.LablCrExpDate1.AutoSize = true;
            this.LablCrExpDate1.Location = new System.Drawing.Point(644, 92);
            this.LablCrExpDate1.Margin = new System.Windows.Forms.Padding(4, 0, 4, 0);
            this.LablCrExpDate1.Name = "LablCrExpDate1";
            this.LablCrExpDate1.Size = new System.Drawing.Size(88, 19);
            this.LablCrExpDate1.TabIndex = 49;
            this.LablCrExpDate1.Text = "تاريخ الانتهاء";
            // 
            // LablCrLssDate1
            // 
            this.LablCrLssDate1.AutoSize = true;
            this.LablCrLssDate1.Location = new System.Drawing.Point(644, 54);
            this.LablCrLssDate1.Margin = new System.Windows.Forms.Padding(4, 0, 4, 0);
            this.LablCrLssDate1.Name = "LablCrLssDate1";
            this.LablCrLssDate1.Size = new System.Drawing.Size(89, 19);
            this.LablCrLssDate1.TabIndex = 48;
            this.LablCrLssDate1.Text = "تاريخ الاصدار";
            // 
            // LablCrNo
            // 
            this.LablCrNo.AutoSize = true;
            this.LablCrNo.Location = new System.Drawing.Point(638, 20);
            this.LablCrNo.Margin = new System.Windows.Forms.Padding(4, 0, 4, 0);
            this.LablCrNo.Name = "LablCrNo";
            this.LablCrNo.Size = new System.Drawing.Size(143, 19);
            this.LablCrNo.TabIndex = 47;
            this.LablCrNo.Text = "رقم السجل التجاري";
            // 
            // panel3
            // 
            this.panel3.Controls.Add(this.label2);
            this.panel3.Controls.Add(this.label1);
            this.panel3.Controls.Add(this.TextBoxRentEndDate);
            this.panel3.Controls.Add(this.TextBoxRentStartDate);
            this.panel3.Controls.Add(this.TextBoxAddress);
            this.panel3.Controls.Add(this.LableAddress);
            this.panel3.Controls.Add(this.TextBoxeFax);
            this.panel3.Controls.Add(this.LableFax);
            this.panel3.Controls.Add(this.TextBoxPhone);
            this.panel3.Controls.Add(this.LablePhone);
            this.panel3.Location = new System.Drawing.Point(300, 305);
            this.panel3.Margin = new System.Windows.Forms.Padding(4, 4, 4, 4);
            this.panel3.Name = "panel3";
            this.panel3.Size = new System.Drawing.Size(753, 146);
            this.panel3.TabIndex = 92;
            // 
            // label2
            // 
            this.label2.AutoSize = true;
            this.label2.Location = new System.Drawing.Point(195, 96);
            this.label2.Margin = new System.Windows.Forms.Padding(4, 0, 4, 0);
            this.label2.Name = "label2";
            this.label2.Size = new System.Drawing.Size(123, 19);
            this.label2.TabIndex = 34;
            this.label2.Text = "تاريخ إنتهاء الإيجار";
            // 
            // label1
            // 
            this.label1.AutoSize = true;
            this.label1.Location = new System.Drawing.Point(628, 96);
            this.label1.Margin = new System.Windows.Forms.Padding(4, 0, 4, 0);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(108, 19);
            this.label1.TabIndex = 33;
            this.label1.Text = "تاريخ بدء الإيجار";
            // 
            // TextBoxRentEndDate
            // 
            this.TextBoxRentEndDate.Location = new System.Drawing.Point(8, 88);
            this.TextBoxRentEndDate.Margin = new System.Windows.Forms.Padding(4, 4, 4, 4);
            this.TextBoxRentEndDate.Name = "TextBoxRentEndDate";
            this.TextBoxRentEndDate.Size = new System.Drawing.Size(176, 27);
            this.TextBoxRentEndDate.TabIndex = 10;
            // 
            // TextBoxRentStartDate
            // 
            this.TextBoxRentStartDate.Location = new System.Drawing.Point(364, 89);
            this.TextBoxRentStartDate.Margin = new System.Windows.Forms.Padding(4, 4, 4, 4);
            this.TextBoxRentStartDate.Name = "TextBoxRentStartDate";
            this.TextBoxRentStartDate.Size = new System.Drawing.Size(247, 27);
            this.TextBoxRentStartDate.TabIndex = 9;
            // 
            // TextBoxAddress
            // 
            this.TextBoxAddress.Location = new System.Drawing.Point(6, 50);
            this.TextBoxAddress.Margin = new System.Windows.Forms.Padding(4, 4, 4, 4);
            this.TextBoxAddress.Name = "TextBoxAddress";
            this.TextBoxAddress.Size = new System.Drawing.Size(604, 27);
            this.TextBoxAddress.TabIndex = 8;
            // 
            // LableAddress
            // 
            this.LableAddress.AutoSize = true;
            this.LableAddress.Location = new System.Drawing.Point(648, 56);
            this.LableAddress.Margin = new System.Windows.Forms.Padding(4, 0, 4, 0);
            this.LableAddress.Name = "LableAddress";
            this.LableAddress.Size = new System.Drawing.Size(46, 19);
            this.LableAddress.TabIndex = 29;
            this.LableAddress.Text = "عنوان";
            // 
            // TextBoxeFax
            // 
            this.TextBoxeFax.Location = new System.Drawing.Point(6, 7);
            this.TextBoxeFax.Margin = new System.Windows.Forms.Padding(4, 4, 4, 4);
            this.TextBoxeFax.Name = "TextBoxeFax";
            this.TextBoxeFax.Size = new System.Drawing.Size(176, 27);
            this.TextBoxeFax.TabIndex = 7;
            // 
            // LableFax
            // 
            this.LableFax.AutoSize = true;
            this.LableFax.Location = new System.Drawing.Point(208, 12);
            this.LableFax.Margin = new System.Windows.Forms.Padding(4, 0, 4, 0);
            this.LableFax.Name = "LableFax";
            this.LableFax.Size = new System.Drawing.Size(50, 19);
            this.LableFax.TabIndex = 27;
            this.LableFax.Text = "فاكس";
            // 
            // TextBoxPhone
            // 
            this.TextBoxPhone.Location = new System.Drawing.Point(363, 9);
            this.TextBoxPhone.Margin = new System.Windows.Forms.Padding(4, 4, 4, 4);
            this.TextBoxPhone.Name = "TextBoxPhone";
            this.TextBoxPhone.Size = new System.Drawing.Size(247, 27);
            this.TextBoxPhone.TabIndex = 6;
            // 
            // LablePhone
            // 
            this.LablePhone.AutoSize = true;
            this.LablePhone.Location = new System.Drawing.Point(652, 16);
            this.LablePhone.Margin = new System.Windows.Forms.Padding(4, 0, 4, 0);
            this.LablePhone.Name = "LablePhone";
            this.LablePhone.Size = new System.Drawing.Size(44, 19);
            this.LablePhone.TabIndex = 25;
            this.LablePhone.Text = "هاتف";
            // 
            // FormBranches
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(9F, 19F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(1334, 722);
            this.Controls.Add(this.panel3);
            this.Controls.Add(this.panel4);
            this.Controls.Add(this.Pan);
            this.Controls.Add(this.ButtonPrevious);
            this.Controls.Add(this.ButtonNext);
            this.Controls.Add(this.ButtonFirst);
            this.Controls.Add(this.ButtonLast);
            this.Controls.Add(this.panel1);
            this.Controls.Add(this.TreeViewMain);
            this.Controls.Add(this.panel2);
            this.Margin = new System.Windows.Forms.Padding(4, 4, 4, 4);
            this.Name = "FormBranches";
            this.RightToLeft = System.Windows.Forms.RightToLeft.Yes;
            this.Text = "FormBranches";
            this.Load += new System.EventHandler(this.FormBranches_Load);
            this.panel1.ResumeLayout(false);
            this.panel1.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.PictureBoxTitle)).EndInit();
            this.panel2.ResumeLayout(false);
            this.panel2.PerformLayout();
            this.Pan.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.re)).EndInit();
            this.panel4.ResumeLayout(false);
            this.panel4.PerformLayout();
            this.panel3.ResumeLayout(false);
            this.panel3.PerformLayout();
            this.ResumeLayout(false);

        }

        #endregion

        private System.Windows.Forms.Panel panel1;
        private System.Windows.Forms.Label lableTitle;
        private System.Windows.Forms.PictureBox PictureBoxTitle;
        private System.Windows.Forms.TreeView TreeViewMain;
        private System.Windows.Forms.Panel panel2;
        private System.Windows.Forms.Button buttonGroupSearch;
        private System.Windows.Forms.RadioButton RadioBtnNotGroup;
        private System.Windows.Forms.RadioButton RadioBtnGroup;
        private System.Windows.Forms.TextBox TextBoxNameLng2;
        private System.Windows.Forms.TextBox TextBoxNameLng1;
        private System.Windows.Forms.Label labelNameLng2;
        private System.Windows.Forms.Label labelNameLng1;
        private System.Windows.Forms.TextBox TextBoxGroupName;
        private System.Windows.Forms.Label labelGroupName;
        private System.Windows.Forms.TextBox TextBoxGroupID;
        private System.Windows.Forms.Label labelGroupID;
        private System.Windows.Forms.TextBox TextBoxID;
        private System.Windows.Forms.Button buttonMainSearch;
        private System.Windows.Forms.TextBox TextBoxMainName;
        private System.Windows.Forms.TextBox TextBoxMainID;
        private System.Windows.Forms.Label labelID;
        private System.Windows.Forms.Label labelMain;
        private System.Windows.Forms.Button ButtonPrevious;
        private System.Windows.Forms.Button ButtonNext;
        private System.Windows.Forms.Button ButtonFirst;
        private System.Windows.Forms.Button ButtonLast;
        private System.Windows.Forms.Panel Pan;
        private System.Windows.Forms.Button ButtonExit;
        private System.Windows.Forms.Button ButtonCancel;
        private System.Windows.Forms.Button ButtonSearch;
        private System.Windows.Forms.Button ButtonDelete;
        private System.Windows.Forms.Button ButtonEdit;
        private System.Windows.Forms.Button ButtonPrint;
        private System.Windows.Forms.Button ButtonSave;
        private System.Windows.Forms.Button ButtonNew;
        private System.Windows.Forms.PictureBox re;
        private System.Windows.Forms.Panel panel4;
        private System.Windows.Forms.TextBox TxtBoxAdvocatteshipExpDate;
        private System.Windows.Forms.TextBox textBox1;
        private System.Windows.Forms.TextBox TxtBoxAdvocatteshipLssDate;
        private System.Windows.Forms.TextBox TxtBoxAdvocatteship;
        private System.Windows.Forms.TextBox TextBoxMunicpExpDate;
        private System.Windows.Forms.TextBox TextBoxMunicpLssDate;
        private System.Windows.Forms.TextBox TextBoxMunicp;
        private System.Windows.Forms.TextBox TextBoxCrExpDate;
        private System.Windows.Forms.TextBox TextBoxCrLssDate;
        private System.Windows.Forms.TextBox TextBoxCrNO;
        private System.Windows.Forms.Label LablCrExpDate3;
        private System.Windows.Forms.Label LablCrLssDate3;
        private System.Windows.Forms.Label LablAdvocateship;
        private System.Windows.Forms.Label LablCrExpDate2;
        private System.Windows.Forms.Label LablCrLssDate2;
        private System.Windows.Forms.Label LablMunicp;
        private System.Windows.Forms.Label LablCrExpDate1;
        private System.Windows.Forms.Label LablCrLssDate1;
        private System.Windows.Forms.Label LablCrNo;
        private System.Windows.Forms.Panel panel3;
        private System.Windows.Forms.Label label2;
        private System.Windows.Forms.Label label1;
        private System.Windows.Forms.TextBox TextBoxRentEndDate;
        private System.Windows.Forms.TextBox TextBoxRentStartDate;
        private System.Windows.Forms.TextBox TextBoxAddress;
        private System.Windows.Forms.Label LableAddress;
        private System.Windows.Forms.TextBox TextBoxeFax;
        private System.Windows.Forms.Label LableFax;
        private System.Windows.Forms.TextBox TextBoxPhone;
        private System.Windows.Forms.Label LablePhone;
    }
}